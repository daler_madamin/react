import { formatNumber } from '../src/helpers/functions';

describe('formatNumber', () => {
  it('should output formatted string of 999.00', () => {
    expect(formatNumber(999, 2)).toBe('999.00');
  });
  it('should output formatted string of 1.00K', () => {
    expect(formatNumber(1000, 2)).toBe('1.00K');
  });
  it('should output formatted string of 1.00M', () => {
    expect(formatNumber(1000001, 2)).toBe('1.00M');
  });
  it('should output formatted string of 1.01M', () => {
    expect(formatNumber(1010000, 2)).toBe('1.01M');
  });
});
