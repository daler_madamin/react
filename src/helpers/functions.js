export const formatNumber = (number, digits) => {
  const map = [
    { value: 1, symbol: '' },
    { value: 1E3, symbol: 'K' },
    { value: 1E6, symbol: 'M' },
    { value: 1E9, symbol: 'B' },
    { value: 1E12, symbol: 'T' },
  ];
  const index = map.findIndex(({ value }) => number < value) - 1;

  return (number / map[index].value).toFixed(digits) + map[index].symbol;
};
