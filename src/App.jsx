import React from 'react';
import Slider from 'react-slick';
import withSizes from 'react-sizes';

import { columns } from './data.json';
import { Container } from './components/Atoms';
import { Column } from './components/Column';

const settings = {
  arrows: false,
  infinite: false,
};

const mapSizesToProps = ({ width }) => ({
  isMobile: width < 768,
});

const App = ({ isMobile }) => (
  <div>
    {isMobile ? (
      <Slider {...settings}>
        {columns.map((data, key) => <Column key={key} data={data} />)}
      </Slider>
    ) : (
      <Container>
        {columns.map((data, key) => <Column key={key} data={data} />)}
      </Container>
    )
    }
  </div>
);

export default withSizes(mapSizesToProps)(App);
