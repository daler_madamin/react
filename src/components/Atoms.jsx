import styled, { css } from 'styled-components';

import { grey100, green } from '../helpers/colors';

const fontSizes = {
  big: '32px',
  medium: '14px',
  small: '12px',
  micro: '10px'
};

export const Container = styled.div`
  display: flex;

  @media screen and (min-width: 768px) {
    display: flex;
    justify-content: space-around;
    width: 700px;
    margin: 0 auto;
  }
  @media screen and (min-width: 1000px) {
    width: 900px;
  }
`;

export const Text = styled.div`
  color: ${({ color }) => color === 'accented' ? green : grey100};
  font-size: ${({ size }) => fontSizes[size] || fontSizes.small};
  ${({ size }) => size === 'big' && css`margin: 5px 0;`}
  ${({ uppercase }) => uppercase && css`text-transform: uppercase;`}
  ${({ bold }) => bold && css`font-weight: bold;`}
`;
