import React from 'react';
import styled from 'styled-components';

import { formatNumber } from '../helpers/functions';
import { Text } from './Atoms';

const Container = styled.div`
  margin: 10px 0;
`;

export const SecKPI = ({ name, value }) => (
  <Container>
    <Text>{name}</Text>
    <Text size="big">{formatNumber(value, 2)}</Text>
  </Container>
);
