import React from 'react';
import styled from 'styled-components';
import { objectOf, oneOfType, number, string } from 'prop-types';

import { Text } from './Atoms';

const Container = styled.div`
  display: flex;
`;

export const KPIFooter = ({ trends }) => (
  <Container>
    {Object.keys(trends).map(key => (
      <div key={key}>
        <Text>{trends[key] || '--'}</Text>
        <Text>{key}</Text>
      </div>
    ))}
  </Container>
);

KPIFooter.propTypes = {
  trends: objectOf(oneOfType([string, number])).isRequired,
};
