import React from 'react';
import styled from 'styled-components';

import { MainKPI } from './MainKPI';
import { SecKPI } from './SecKPI';
import { Text } from './Atoms';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 20px 10px;

  text-align: center;

  & > ${Text} {
    margin-bottom: 20px;
  }
`;

export const Column = ({ data }) => (
  <Container>
    <Text size="medium" uppercase bold>{data.name}</Text>
    <MainKPI {...data.main_kpi} />
    {data.secondary_kpis.map((data, key) => <SecKPI key={key} {...data} />)}
  </Container>
);
