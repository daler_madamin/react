import React from 'react';
import { string, number, objectOf, oneOfType } from 'prop-types';
import styled from 'styled-components';

import { formatNumber } from '../helpers/functions';
import { grey100 } from '../helpers/colors';
import { Text } from './Atoms';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-around;
  width: 160px;
  height: 160px;
  margin-bottom: 20px;
  padding: 10px;
  background: white;
  border: 2px solid ${grey100};
  border-radius: 50%;
  box-shadow: 3px 1px 5px rgba(0, 0, 0, .25);
`;

const KPIFooter = styled.div`
  display: flex;
  justify-content: space-between;

  & > div:last-child {
    margin-left: 10px;
  }
`;

export const MainKPI = ({ name, value, trends }) => (
  <Container>
    <Text>{name}</Text>
    <Text size="big">{formatNumber(value, 2)}</Text>
    <KPIFooter>
      {Object.keys(trends).map(key => (
        <div key={key}>
          <Text
            color={trends[key] ? 'accented' : null}
            size="medium"
            bold
          >
            {trends[key] ? `${trends[key]} %` : '--'}
          </Text>
          <Text>{key}</Text>
        </div>
      ))}
    </KPIFooter>
  </Container>
);

MainKPI.propTypes = {
  name: string.isRequired,
  value: number.isRequired,
  trends: objectOf(oneOfType([string, number])),
};

MainKPI.defaultProps = {
  trends: {},
};
